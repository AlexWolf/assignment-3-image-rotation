#include "image.h"


enum image_status create_image(struct image *img, uint64_t width, uint64_t height) {
    struct pixel* data = malloc(width * height * PIXEL_SIZE);
    if (!data)
        return IMG_ERROR;
    *img = (struct image) {
        .width = width,
        .height = height,
        .data = data
    };
    return IMG_OK;
}

void free_image(struct image* source) {
    if (source->data) {
        free(source->data);
        source->data = NULL;
    }
}
