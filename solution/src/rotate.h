#pragma once
#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"


/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate( struct image const source );
#endif
