#include "rotate.h"
#include "bmp_io.h"


#define SUCCESS 0
#define ERR 1

#define ARGUMENTS_NEEDED 4
#define INPUT_FILE 1
#define OUTPUT_FILE 2
#define ANGLES 3

#define READ_MODE "rb"
#define WRITE_MODE "wb"


int main( int argc, char** argv ) {
    if (argc != ARGUMENTS_NEEDED) {
        fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image> <angle>\n");
        return ERR;
    }

    FILE* input;
    FILE* output;
    if (file_open(&input, argv[INPUT_FILE], READ_MODE) != OPEN_OK || file_open(&output, argv[OUTPUT_FILE], WRITE_MODE) != OPEN_OK) {
        fprintf(stderr, "Error: input/output file opening\n");
        return ERR;
    }
    
    int angle = (atoi(argv[ANGLES]) % 360 + 360) % 360;
    if (angle % 90) {
        fprintf(stderr, "Error: Allowed angles: 0, 90, -90, 180, -180, 270, -270\n");
        return ERR;
    }
    angle /= 90;

    struct image source;
    if (from_bmp(input, &source) != READ_OK) {
        fprintf(stderr, "Error: read image\n");
        if (source.data != NULL)
            free_image(&source);
        return ERR;
    }    
        
    struct image rotated = source;
    for (int i = 0; i < angle; i++)
        rotated = rotate(rotated);
        
    if (to_bmp(output, &rotated) != WRITE_OK) {
        fprintf(stderr, "Error: write image\n");
        if (rotated.data != NULL)
            free_image(&rotated);
        return ERR;
    }
    
    free_image(&rotated);
    
    if (file_close(input) != CLOSE_OK || file_close(output) != CLOSE_OK) {
        fprintf(stderr, "Error: input/output file closing\n");
        return ERR;
    }
    
    return SUCCESS;
}
