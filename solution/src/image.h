#pragma once
#ifndef IMAGE_H
#define IMAGE_H

#include <malloc.h>
#include <stdint.h>
#include <stdlib.h>


#define PIXEL_SIZE sizeof(struct pixel)


struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum  image_status {
  IMG_OK = 0,
  IMG_ERROR
};

enum image_status create_image(struct image *img, uint64_t width, uint64_t height);
void free_image(struct image* source);
#endif
