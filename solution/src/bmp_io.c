#include "bmp_io.h"


enum open_status file_open(FILE** in, const char *pathname, const char *mode) {
    *in = fopen(pathname, mode);
    if (!*in)
        return OPEN_ERROR;
    return OPEN_OK;
}


enum close_status file_close(FILE *out) {
    if (!out)
        return CLOSE_ERROR;
    return fclose(out) ? CLOSE_ERROR : CLOSE_OK;
}


uint8_t get_padding(uint64_t width) {
    return BMP_BYTE_PADDING - (width * PIXEL_SIZE % BMP_BYTE_PADDING);
}


struct bmp_header get_header(const struct image* source) {
    uint64_t width = source->width;
    uint64_t height = source->height;
    uint64_t size_image = height * (width * PIXEL_SIZE + get_padding(width));
    return (struct bmp_header) {
            .bfType = BMP_TYPE,
            .bfileSize = BMP_OFFBITS + size_image,
            .bfReserved = BMP_RESERVED,
            .bOffBits = BMP_OFFBITS,
            .biSize = BMP_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BYTE_TO_BITS * PIXEL_SIZE,
            .biCompression = BMP_COMPRESSION,
            .biSizeImage = size_image,
            .biXPelsPerMeter = BMP_X_PELS_PER_METER,
            .biYPelsPerMeter = BMP_Y_PELS_PER_METER,
            .biClrUsed = BMP_CLR_USED,
            .biClrImportant = BMP_CLR_IMPORTANT
    };
}


enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;
    if (!fread(&header, BMP_OFFBITS, 1, in) || header.bfType != BMP_TYPE || header.biBitCount != BYTE_TO_BITS * PIXEL_SIZE)
        return READ_INVALID_HEADER;
    if (fseek(in, header.bOffBits, SEEK_SET))
        return READ_INVALID_HEADER;

    uint64_t width = header.biWidth;
    uint64_t height = header.biHeight;
    if (create_image(img, width, height) != IMG_OK)
        return READ_ALLOCATION_ERROR;
    uint8_t padding = get_padding(width);
    
    for (uint64_t i = 0; i < height * width; i += width) {
        if (fread(&img->data[i], PIXEL_SIZE, width, in)-width)
            return READ_INVALID_BITS;
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}


enum write_status to_bmp( FILE* out, struct image const* img ) { 
    struct bmp_header header = get_header(img);
    if (!fwrite(&header, BMP_OFFBITS, 1, out))
        return WRITE_ERROR;
    
    uint64_t width = header.biWidth;    
    uint64_t zero = 0;
    uint64_t padding = get_padding(width);
    for (uint64_t i = 0; i < header.biHeight * width; i += width) {
        if (fwrite(&img->data[i], PIXEL_SIZE, width, out)-width)
            return WRITE_ERROR;
        fwrite(&zero, sizeof(uint8_t), padding, out);
    }
    return WRITE_OK;
}
