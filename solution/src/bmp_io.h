#pragma once
#ifndef BMP_IO_H
#define BMP_IO_H

#include "image.h"
#include <stdio.h>


#define BMP_BYTE_PADDING 4
#define BMP_OFFBITS sizeof(struct bmp_header)
#define BMP_TYPE 0x4D42
#define BMP_RESERVED 0
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BYTE_TO_BITS 8
#define BMP_COMPRESSION 0
#define BMP_X_PELS_PER_METER 2835
#define BMP_Y_PELS_PER_METER 2835
#define BMP_CLR_USED 0
#define BMP_CLR_IMPORTANT 0

#pragma pack(push, 1)
struct bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)

enum open_status {
  OPEN_OK = 0,
  OPEN_ERROR
};

enum open_status file_open(FILE **in, const char *pathname, const char *mode);

enum close_status {
  CLOSE_OK = 0,
  CLOSE_ERROR
};

enum close_status file_close(FILE *out);


/*  deserializer   */
enum read_status  {
  READ_OK = 0,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ALLOCATION_ERROR
  /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif
