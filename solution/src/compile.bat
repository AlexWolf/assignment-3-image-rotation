del /q image-transformer.exe
del /q bmp_io.o
del /q image.o
del /q rotate.o
del /q main.o
gcc -c bmp_io.c -o bmp_io.o
gcc -c image.c -o image.o
gcc -c rotate.c -o rotate.o
gcc -c main.c -o main.o
gcc bmp_io.o image.o rotate.o main.o -o image-transformer.exe
image-transformer.exe ../../tester/tests/1/input.bmp ../../tester/tests/1/output.bmp -90
image-transformer.exe ../../tester/tests/2/input.bmp ../../tester/tests/2/output.bmp 270
image-transformer.exe ../../tester/tests/3/input.bmp ../../tester/tests/3/output.bmp 270
image-transformer.exe ../../tester/tests/4/input.bmp ../../tester/tests/4/output.bmp 90
image-transformer.exe ../../tester/tests/5/input.bmp ../../tester/tests/5/output.bmp 180
image-transformer.exe ../../tester/tests/6/input.bmp ../../tester/tests/6/output.bmp 0
pause