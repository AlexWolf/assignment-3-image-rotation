#include "rotate.h"


struct image rotate( struct image const source ) {
    struct image image_rotated;
    if (create_image(&image_rotated, source.height, source.width) != IMG_OK)
        return image_rotated;
    for (uint64_t height = 0; height < source.height; height++){
        for (uint64_t width = 0; width < source.width; width++)
            image_rotated.data[(source.width - width - 1) * image_rotated.width + height] = source.data[height * source.width + width];
    }
    struct image src = source;
    free_image(&src);
    return image_rotated;
}
